FROM openjdk:11-jre-slim

COPY target/naming-server-0.0.1-SNAPSHOT.jar /naming-server.jar

EXPOSE 8000

ENTRYPOINT ["java", "-jar", "/naming-server.jar"]
